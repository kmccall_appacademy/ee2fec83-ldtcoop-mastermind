class Code
  # init, accessors, constants, class, instance
  def initialize(pegs_arr)
    @pegs = pegs_arr
  end

  attr_reader :pegs

  def [](pos)
    @pegs[pos]
  end

  PEGS = {
    "R" => "Red", "G" => "Green", "B" => "Blue",
    "Y" => "Yellow", "O" => "Orange", "P" => "Purple"
  }.freeze

  # class methods

  def self.parse(input)
    peg_seq = input.upcase.chars.map do |el|
      raise "Invalid color in input" unless PEGS.key?(el)
      el
    end
    Code.new(peg_seq)
  end

  def self.random
    colors = ["R", "G", "B", "Y", "O", "P"]
    sequence = ""
    4.times do
      sequence += colors[rand(0..5)]
    end
    Code::parse(sequence)
  end

  # instance methods
  def exact_matches(guess)
    matches = 0
    guess.pegs.each_with_index do |el, ind|
      matches += 1 if el == @pegs[ind]
    end
    matches
  end

  def near_matches(guess)
    inter_set = self.pegs.uniq & guess.pegs.uniq
    (inter_set.length - self.exact_matches(guess)).abs
  end

  def ==(other)
    if other.is_a? Code
      self.pegs == other.pegs
    else
      false
    end
  end

end

class Game
  def initialize(secret = Code::random)
    @secret_code = secret
    @guess_history = []
  end

  attr_reader :secret_code, :guess_history

  def get_guess
    puts "What is your guess? Ex: 'RGBY'"
    begin
      guess = gets.chomp
      Code::parse(guess)
    rescue
      puts "Looks like you inputted something that wasn't a color!"
      puts "Enter a valid combination and try again."
      retry
    end
  end

  def display_matches(guess)
    print "Exact matches: #{@secret_code.exact_matches(guess)}"
    print "Near matches: #{@secret_code.near_matches(guess)}"
  end

  def self.play
    puts "Press ENTER to begin the game"
    playthru = Game.new
    loop do
      # loss condition
      if playthru.guess_history.length == 10
        puts "Sorry, you are out of turns."
        puts "The code was #{playthru.secret_code.pegs}"
        break
      # win condition
      elsif playthru.guess_history.last == playthru.secret_code
        puts "You won!"
        puts "The code was #{playthru.secret_code.pegs}"
        break
      end
      # gameplay
      guess_history.each do |el|
        p el.pegs
        playthru.display_matches(el)
      end
      playthru.guess_history.push(get_guess)
    end
    puts "Would you like to play again (y/n)?"
  end
end

play_game = true
while play_game
  Game::play
  loop do
    input = gets.chomp
    if input.upcase == "Y"
      break
    elsif input.upcase == "N"
      puts "Thanks for playing!"
      play_game = false
      break
    else
      puts "Sorry, what was that?"
    end
  end
end
